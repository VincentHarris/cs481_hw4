﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using CS481_HW4.Model;
using CS481_HW4.SnesViewModel;

namespace CS481_HW4.SnesView
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SnesView : ContentPage
    {
        SnesViewModel.SnesViewModel vm;
        public SnesView()
        {
           // InitializeComponent();
            vm = new SnesViewModel.SnesViewModel();
            BindingContext = vm;
        }

        private void RemoveClicked(object sender, EventArgs e)
        {
            var button = sender as Button;

            var snes = button?.BindingContext as Snes;

            var vm = BindingContext as SnesViewModel.SnesViewModel;

            vm?.RemoveCommand.Execute(snes);
        }
        //References https://www.youtube.com/watch?v=Et6KCB1zeX8&t=345s
        public async void ItemTapped(object sender, ItemTappedEventArgs e)
       {
           var gamedetails = e.Item as Snes;
         await Navigation.PushAsync(new gameDetails(gamedetails.GameName, gamedetails.Topic, gamedetails.ShortDesc, gamedetails.Image, gamedetails.Image2));
         }

    }
}