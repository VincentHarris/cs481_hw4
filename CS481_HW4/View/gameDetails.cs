﻿using Xamarin.Forms;

namespace CS481_HW4.SnesView
{
    internal class gameDetails : Page
    {
        private string gameName;
        private string topic;
        private string shortDesc;
        private string image;
        private string image2;

        public gameDetails(string gameName, string topic, string shortDesc, string image, string image2)
        {
            this.gameName = gameName;
            this.topic = topic;
            this.shortDesc = shortDesc;
            this.image = image;
            this.image2 = image2;
        }
    }
}