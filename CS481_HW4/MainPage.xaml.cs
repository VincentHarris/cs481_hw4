﻿using CS481_HW4.Model;
using CS481_HW4.SnesViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CS481_HW4
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {

        public MainPage()
        {
            InitializeComponent();
        }

        private void RemoveClicked(object sender, EventArgs e)
        {
            var button = sender as Button;

           var snes = button?.BindingContext as Snes;

            var vm = BindingContext as SnesViewModel.SnesViewModel;

            vm?.RemoveCommand.Execute(snes);
        }

        //private async void RefreshCommand(object sender, EventArgs e)
        // {
        //  await LoadDataAsync();
        //}
        //private void ListView_OnItemTapped(object sender, EventArgs e)
        //{
        // var vm = BindingContext as SnesViewModel;
        //var games = e.Item as Games;
        //vm.HideorShowGame(games);
        //}
    }
}
