﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;
using CS481_HW4.Model;
using CS481_HW4.SnesView;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace CS481_HW4.SnesViewModel
{
    public class SnesViewModel
    {
        //public List<Snes> Games { get; set; }
        //Referenced from https://www.youtube.com/watch?reload=9&v=5Vk_08CEfZQ
        public ObservableCollection<Snes> Games { get; set; }

        public Command<Snes> RemoveCommand
        {
            get 
            {
                return new Command<Snes>((snes) =>
                { Games.Remove(snes); });
            }
        }
        public SnesViewModel()
        {
            Games = new ObservableCollection<Snes>();
            {
                Games.Add(new Snes
                {
                    GameName = "Final Fantasy VII",
                    Topic = "The Cyberpunk JRPG that redefined the genre!",
                    Image = "ff7.jpg",
                    Image2 = "ff72.jpg",
                    ShortDesc = "Final Fantasy VII is the series' first foray into 3D gaming. Take control of Cloud Strife, EX-SOLDIER First Class as he, his childhood friend, Tifa Lockhart and brash eco-terrorist group (AVALANCHE) leader Barrett Wallace as they fight the evil mega-corporation Shinra! Shinra is sucking up the planet's life blood"

                }
                );

                Games.Add(new Snes
                {
                    GameName = "Final Fantasy VIII",
                    Topic = "A Final Fantasy that battles Time!",
                    Image = "ff8.jpg",
                    Image2 = "ff82.jpg",
                    ShortDesc = "Final Fantasy VIII allows you to take control of Squall Leonheart, a taciturn lone wolf who specialized in the incredibly hard to handle Gunblade. Squall attends Balamb Garden military academy with fellow students Zell Dincht, Selphie Tilmitt, Quistis Trepe and his rival gunblade user Seifer Almasy. Summon the mighty strength of the Guardian Forces, which are deities that command the elements! Find out why Squall and company were stricken amnesia, and stop the evil time witch and her plan to end all timelines with time compression!"

                }
                );

                Games.Add(new Snes
                {
                    GameName = "Final Fantasy IX",
                    Topic = "A Final Fantasy that is a return to form!",
                    Image = "ff9.jpg",
                    Image2 = "ff92.jpg",
                    ShortDesc = "Final Fantasy IX was a return the series' roots in renaissance like fantasy! Steamship soared through the skies being powered by a mysterious magical essence known as 'The Mist'. You play as Zidane Tribal, a member in the theatre group/ thieve guild called Tantalus. On one fateful 'thief' job, the crew kidnaps the Princess Garnet Til Alexandros XIII and this event starts a cascade of events that leaves the fate of the planet uncertain. Will you help Zidane find out who or what he is? Will you and your new crew including the black mage Vivi who doesn't know if he's real, Steiner, a knight sworn to protect the princess and Freya, a honorable dragoon searching for her lost love to save the the planet they call home!",

                }
               );

                Games.Add(new Snes
                {
                    GameName = "Final Fantasy X",
                    Topic = "The first Final Fantasy that shows emotions on faces!",
                    Image = "ffx.jpg",
                    Image2 = "ffx2.jpg",
                    ShortDesc = "Final Fantasy X is a perfect blend of high fantasy and ancient technology. You play as Tidus, star blitzball player of the Zanarkand Abes. During the Jecht memorial cup, a giant entity known as Sin attacks Zanarkand and sends Tidus to a faraway land/world! Tidus meets Wakka, a retiring Blitzball player, Lulu, a black magic specialist, and Yuna, who's a summoner and daughter of one the most renowned summoners in their world! On this journey the team will discover the dark secrets that world's religion is based upon, the truth about Sin and Tidus' arrival in Spira!",

                }
               );

            }
        }

       async private void RefreshListView()
        {
          // SnesViewModel = await
        }

        private bool _isRefreshing = false;
        public bool IsRefreshing
        {
            get
            { return _isRefreshing; }
            set
            {
                _isRefreshing = value;
                OnPropertyChanged(nameof(IsRefreshing));
            }
        }
       private void OnPropertyChanged(string v)
        {
            throw new NotImplementedException();
        }

    }
}
    
